# SAE_Image



# Qestion A.0: 
    A.0:
        Les valeurs présentes dans l'aperçu donné correspondent à l'entête de fichier, l'entête du bitmap et le corps de l'image. Un msg d'erreur s'affiche car la taille de l'image spécifiée après les deux premiers octets ne correpond pas au poids d'origine.
            -   D'après les valeurs on a une taille de 800 Ko environ, or le poids d'origine est de 810 Ko environ.
        On change alors les valeurs pour que la taille corresponde.

![Img_SAE_Image/entete.png](Img_SAE_Image/entete.png "entete.png")

![Img_SAE_Image/Erreur.png](Img_SAE_Image/Erreur.png "Erreur.png")

![Img_SAE_Image/entete_correcte.png](Img_SAE_Image/entete_correcte.png "entete_correcte.png")

![Img_SAE_Image/valide_ouverture](Img_SAE_Image/valide_ouverture.png "valide_ouverture.png")


# Cheminement :
    J'ai tenté une première modification dans Okteta en remplacant le codage de la taille par la bonne valeur cependant le message d'erreur s'affichait toujours. Je me suis corrigé cette fois-ci en codant en little Endian puis dans dans la quatrième capture l'image s'ouvre sans ce message d'erreur.

# Question A.1:
    A.1:
        
    

    